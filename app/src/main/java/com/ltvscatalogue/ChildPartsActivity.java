package com.ltvscatalogue;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import APIInterface.CategoryAPI;
import Adapter.ServicePartadapter;
import Adapter.Serviceadaptertwo;
import Model.FullUnitsearch.Fullunitsearch;
import Model.FullUnitsearch.Fullunitsearchlist;
import Model.FullUnitsearch.SerFullunitsearch;
import Model.FullUnitsearch.Serfullunitsearchlist;
import Model.Wheretouse.ChildPartsAdapter;
import Model.Wheretouse.ChildPartsAdaptertwo;
import Model.Wheretouse.FullUnitItem;
import Model.Wheretouse.GetChildDetails;
import RetroClient.RetroClient;
import alertbox.Alertbox;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChildPartsActivity extends AppCompatActivity {
    ListView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    ChildPartsAdapter adapter;
    ChildPartsAdaptertwo serviceadapter2;
    String Partnumber;
    List<GetChildDetails> Fullunitsearchlist;
    List<FullUnitItem> SerFullunitsearchlist;
    private ArrayList<String> sino;
    int count = 1;
    private Button btn_prev;
    private Button btn_next;
    private int pageCount;
    public int NUM_ITEMS_PAGE = 8;
    public int TOTAL_LIST_ITEMS = 0;
    private int increment = 0;
    public int val = 0;
    LinearLayout Pagination;
    TextView Root;
    NetworkConnection net;
    private Alertbox box = new Alertbox(ChildPartsActivity.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_parts);

        net = new NetworkConnection(ChildPartsActivity.this);
        Root = (TextView) findViewById(R.id.root1);
        sino = new ArrayList<>();
        Fullunitsearchlist = new ArrayList<>();
        SerFullunitsearchlist = new ArrayList<>();
        Intent i = getIntent();
        Partnumber = i.getStringExtra("childnumber");
        Root.setText(Partnumber);
        recyclerView =  findViewById(R.id.card_recycler_view);
        checkInternet();
        Pagination = (LinearLayout) findViewById(R.id.Pagination);
        btn_prev = (Button) findViewById(R.id.prev);
        btn_next = (Button) findViewById(R.id.next);
        btn_prev.setVisibility(View.GONE);

        btn_next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                increment++;
                checkInternet();
                CheckEnable();
            }
        });
        btn_prev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                increment--;
                checkInternet();
                CheckEnable();
            }
        });
    }

    private void CheckEnable() {
        try {
//            TOTAL_LIST_ITEMS = Fullunitsearchlist.size();
            val = TOTAL_LIST_ITEMS % NUM_ITEMS_PAGE;
            val = val == 0 ? 0 : 1;
            pageCount = TOTAL_LIST_ITEMS / NUM_ITEMS_PAGE + val;
            if (increment == pageCount-1) {
                btn_next.setVisibility(View.GONE);
            } else if (increment == 0) {
                btn_prev.setVisibility(View.GONE);
            } else {
                btn_prev.setVisibility(View.VISIBLE);
                btn_next.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
//            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void checkInternet() {
        if (net.CheckInternet()) {
            getpartdetails();
        } else {
            box.showAlertboxwithback("Please check your network connection and try again!");
        }
    }
    public void getpartdetails() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ChildPartsActivity.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            final CategoryAPI service = RetroClient.getApiService();

            Call<GetChildDetails> call = service.fullunititem(Partnumber);
            call.enqueue(new Callback<GetChildDetails>() {
                @Override
                public void onResponse(Call<GetChildDetails> call, Response<GetChildDetails> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("success")) {

//                        layoutManager = new LinearLayoutManager(getApplicationContext());
//                        recyclerView.setLayoutManager(layoutManager);
                        ChildPartsAdapter adapter =new ChildPartsAdapter(ChildPartsActivity.this,response.body().getData());
                       recyclerView.setAdapter(adapter);

                    } else if (response.body().getResult().equals("ServiceSuccess")) {
//                        getservicedetails();
                    } else {
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                                ChildPartsActivity.this).create();

                        LayoutInflater inflater = (ChildPartsActivity.this).getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.emptypartalert, null);
                        alertDialog.setView(dialogView);
                        Button Ok = (Button) dialogView.findViewById(R.id.ok);
                        final TextView Message = (TextView) dialogView.findViewById(R.id.msg);
                        final TextView send = (TextView) dialogView.findViewById(R.id.toenquiry);
                        Message.setText("No Parts Available !");
                        Ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(ChildPartsActivity.this, HomeActivity.class));
                                alertDialog.dismiss();
                            }
                        });
                        send.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(ChildPartsActivity.this, SendEnquiry.class));
                                alertDialog.dismiss();
                            }
                        });
                        alertDialog.show();
                        progressDialog.dismiss();
                        Toast.makeText(ChildPartsActivity.this, "No Record Found", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<GetChildDetails> call, Throwable t) {
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                            ChildPartsActivity.this).create();

                    LayoutInflater inflater = (ChildPartsActivity.this).getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.emptypartalert, null);
                    alertDialog.setView(dialogView);
                    Button Ok = (Button) dialogView.findViewById(R.id.ok);
                    final TextView Message = (TextView) dialogView.findViewById(R.id.msg);
                    final TextView send = (TextView) dialogView.findViewById(R.id.toenquiry);
                    Message.setText("No Parts Available !");
                    Ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(ChildPartsActivity.this, HomeActivity.class));
                            alertDialog.dismiss();
                        }
                    });
                    send.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(ChildPartsActivity.this, SendEnquiry.class));
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.show();
                    progressDialog.dismiss();
                    Toast.makeText(ChildPartsActivity.this, "No Record Found", Toast.LENGTH_SHORT).show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Poor Network..", Toast.LENGTH_LONG).show();
        }
    }



    public void Home(View view) {
        startActivity(new Intent(this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    }
