package alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;




public class Alertbox {
	Context context;
	
	public Alertbox(Context con) {
		// TODO Auto-generated constructor stub
		this.context = con;
	}
	

	public void showAlertbox(String msg)
	{

		final AlertDialog alertDialog = new Builder(context).create();
		alertDialog.setTitle("LUCAS TVS");
		alertDialog.setMessage(msg);
		alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				// TODO Auto-generated method stub
				alertDialog.dismiss();

			}
		});
		alertDialog.show();

	}


	public void showAlertboxwithback(String msg)
	{
		final AlertDialog alertDialog = new Builder(context).create();
		alertDialog.setTitle("LUCAS TVS");
		alertDialog.setMessage(msg);
		alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				// TODO Auto-generated method stub
				((Activity) context).onBackPressed();

			}
		});
		alertDialog.show();

	}
}
