
package Model.Dealetstatefilter;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
@Entity(tableName = "servicedealer")
public class Dealerstatedetails {

    @SerializedName("Address")
    @ColumnInfo(name = "Address")
    private String mAddress;
    @Ignore
    @SerializedName("Address1")
    @ColumnInfo(name = "Address1")
    private String mAddress1;
    @Ignore
    @SerializedName("Allocation")
    @ColumnInfo(name = "Allocation")
    private String mAllocation;
    @Ignore
    @SerializedName("AuditorName_admin")
    @ColumnInfo(name = "AuditorName_admin")
    private String mAuditorNameAdmin;
    @Ignore
    @SerializedName("AuditorName_service")
    @ColumnInfo(name = "AuditorName_service")
    private String mAuditorNameService;
    @Ignore
    @SerializedName("AuditorName_tool")
    @ColumnInfo(name = "AuditorName_tool")
    private String mAuditorNameTool;
    @Ignore
    @SerializedName("AuditorName_warrenty")
    @ColumnInfo(name = "AuditorName_warrenty")
    private String mAuditorNameWarrenty;
    @Ignore
    @SerializedName("AuditorName_work")
    @ColumnInfo(name = "AuditorName_work")
    private String mAuditorNameWork;
    @Ignore
    @SerializedName("Category")
    @ColumnInfo(name = "Category")
    private String mCategory;
    @SerializedName("City")
    @ColumnInfo(name = "City")
    private String mCity;
    @SerializedName("Contactperson")
    @ColumnInfo(name = "Contactperson")
    private String mContactperson;
    @Ignore
    @SerializedName("Date")
    @ColumnInfo(name = "Date")
    private String mDate;
    @SerializedName("Dealerid")
    @ColumnInfo(name = "Dealerid")
    private String mDealerid;
    @SerializedName("Dealername")
    @ColumnInfo(name = "Dealername")
    private String mDealername;
    @SerializedName("Depocode")
    @ColumnInfo(name = "Depocode")
    private String mDepocode;
    @SerializedName("Depotname")
    @ColumnInfo(name = "Depotname")
    private String mDepotname;
    @SerializedName("Email_id")
    @ColumnInfo(name = "Email_id")
    private String mEmailId;
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    @ColumnInfo(name = "id")
    private Long mId;
    @SerializedName("Landlineno")
    @ColumnInfo(name = "Landlineno")
    private String mLandlineno;
    @SerializedName("Mobileno")
    @ColumnInfo(name = "Mobileno")
    private String mMobileno;
    @Ignore
    @SerializedName("Pincode")
    @ColumnInfo(name = "Pincode")
    private Long mPincode;
    @SerializedName("Region")
    @ColumnInfo(name = "Region")
    private String mRegion;
    @SerializedName("Reportingtvsgroup")
    @ColumnInfo(name = "Reportingtvsgroup")
    private String mReportingtvsgroup;
    @SerializedName("sno")
    @ColumnInfo(name = "sno")
    private String mSno;
    @SerializedName("State")
    @ColumnInfo(name = "State")
    private String mState;
    @SerializedName("Status")
    @ColumnInfo(name = "Status")
    @Ignore
    private String mStatus;
    @Ignore
    @SerializedName("TotalAvg")
    @ColumnInfo(name = "TotalAvg")
    private Long mTotalAvg;
    @Ignore
    @SerializedName("TotalRating")
    @ColumnInfo(name = "TotalRating")
    private Long mTotalRating;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getAddress1() {
        return mAddress1;
    }

    public void setAddress1(String Address1) {
        mAddress1 = Address1;
    }

    public String getAllocation() {
        return mAllocation;
    }

    public void setAllocation(String Allocation) {
        mAllocation = Allocation;
    }

    public String getAuditorNameAdmin() {
        return mAuditorNameAdmin;
    }

    public void setAuditorNameAdmin(String AuditorNameAdmin) {
        mAuditorNameAdmin = AuditorNameAdmin;
    }

    public String getAuditorNameService() {
        return mAuditorNameService;
    }

    public void setAuditorNameService(String AuditorNameService) {
        mAuditorNameService = AuditorNameService;
    }

    public String getAuditorNameTool() {
        return mAuditorNameTool;
    }

    public void setAuditorNameTool(String AuditorNameTool) {
        mAuditorNameTool = AuditorNameTool;
    }

    public String getAuditorNameWarrenty() {
        return mAuditorNameWarrenty;
    }

    public void setAuditorNameWarrenty(String AuditorNameWarrenty) {
        mAuditorNameWarrenty = AuditorNameWarrenty;
    }

    public String getAuditorNameWork() {
        return mAuditorNameWork;
    }

    public void setAuditorNameWork(String AuditorNameWork) {
        mAuditorNameWork = AuditorNameWork;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String Category) {
        mCategory = Category;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public String getContactperson() {
        return mContactperson;
    }

    public void setContactperson(String Contactperson) {
        mContactperson = Contactperson;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String Date) {
        mDate = Date;
    }

    public String getDealerid() {
        return mDealerid;
    }

    public void setDealerid(String Dealerid) {
        mDealerid = Dealerid;
    }

    public String getDealername() {
        return mDealername;
    }

    public void setDealername(String Dealername) {
        mDealername = Dealername;
    }

    public String getDepocode() {
        return mDepocode;
    }

    public void setDepocode(String Depocode) {
        mDepocode = Depocode;
    }

    public String getDepotname() {
        return mDepotname;
    }

    public void setDepotname(String Depotname) {
        mDepotname = Depotname;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String EmailId) {
        mEmailId = EmailId;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLandlineno() {
        return mLandlineno;
    }

    public void setLandlineno(String Landlineno) {
        mLandlineno = Landlineno;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String Mobileno) {
        mMobileno = Mobileno;
    }

    public Long getPincode() {
        return mPincode;
    }

    public void setPincode(Long Pincode) {
        mPincode = Pincode;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String Region) {
        mRegion = Region;
    }

    public String getReportingtvsgroup() {
        return mReportingtvsgroup;
    }

    public void setReportingtvsgroup(String Reportingtvsgroup) {
        mReportingtvsgroup = Reportingtvsgroup;
    }

    public String getSno() {
        return mSno;
    }

    public void setSno(String sno) {
        mSno = sno;
    }

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String Status) {
        mStatus = Status;
    }

    public Long getTotalAvg() {
        return mTotalAvg;
    }

    public void setTotalAvg(Long TotalAvg) {
        mTotalAvg = TotalAvg;
    }

    public Long getTotalRating() {
        return mTotalRating;
    }

    public void setTotalRating(Long TotalRating) {
        mTotalRating = TotalRating;
    }

}
