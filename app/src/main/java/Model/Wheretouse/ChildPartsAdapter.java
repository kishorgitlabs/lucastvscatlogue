package Model.Wheretouse;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.ltvscatalogue.ChildPartsActivity;
import com.ltvscatalogue.Partdetails;
import com.ltvscatalogue.R;
import com.ltvscatalogue.WheretoUserPartDeatils;

import java.util.ArrayList;
import java.util.List;

public class ChildPartsAdapter extends ArrayAdapter{
    List<FullUnitItem> Fullunitsearchlist= new ArrayList<>();
    Context context;
    public ChildPartsAdapter(ChildPartsActivity context, List<FullUnitItem> data) {
        super(context, R.layout.fullunitxml);
        this.context=context;
        this.Fullunitsearchlist=data;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(getContext());
        convertView = inflater.inflate(R.layout.fullunitxml, parent, false);

        TextView sno =(TextView) convertView.findViewById(R.id.sino1);
        final TextView partno =(TextView) convertView.findViewById(R.id.partno);
        TextView descrip =(TextView) convertView.findViewById(R.id.descrip);
        TextView nooff =(TextView) convertView.findViewById(R.id.nooff);

        sno.setText(position+1+"");
        partno.setText(Fullunitsearchlist.get(position).getPartNo());
        descrip.setText(Fullunitsearchlist.get(position).getProductname());
        nooff.setText(Fullunitsearchlist.get(position).getUos());

partno.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent i = new Intent(context, WheretoUserPartDeatils.class);
        i.putExtra("childPartNumber", partno.getText().toString().trim());
        context.startActivity(i);
    }
});


        return convertView;
    }

    @Override
    public int getCount() {
        return Fullunitsearchlist.size();
    }
}
