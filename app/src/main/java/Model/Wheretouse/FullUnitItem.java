package Model.Wheretouse;

import com.google.gson.annotations.SerializedName;

public class FullUnitItem {

	@SerializedName("Description")
	private String description;

	@SerializedName("Mrp")
	private double mrp;

	@SerializedName("OldMrp")
	private double oldMrp;

	@SerializedName("Pro_exp_iamge")
	private String proExpIamge;

	@SerializedName("App_id")
	private String appId;

	@SerializedName("Plant")
	private String plant;

	@SerializedName("Pro_type")
	private String proType;

	@SerializedName("expoledview")
	private int expoledview;

	@SerializedName("Productname")
	private String productname;

	@SerializedName("Oem_partno")
	private String oemPartno;

	@SerializedName("Part_Volt")
	private String partVolt;

	@SerializedName("Unittype")
	private String unittype;

	@SerializedName("GST")
	private int gST;

	@SerializedName("Full_Unit_No")
	private String fullUnitNo;

	@SerializedName("Part_Outputrng")
	private String partOutputrng;

	@SerializedName("OEname")
	private String oEname;

	@SerializedName("Oem_id")
	private String oemId;

	@SerializedName("Pro_id")
	private String proId;

	@SerializedName("Pro_supersed")
	private String proSupersed;

	@SerializedName("Engin_type")
	private String enginType;

	@SerializedName("Part_No")
	private String partNo;

	@SerializedName("Uos")
	private String uos;

	@SerializedName("Pro_image")
	private String proImage;

	@SerializedName("Id")
	private int id;

	@SerializedName("Pro_model")
	private String proModel;

	@SerializedName("Appname")
	private String appname;

	@SerializedName("HSNCode")
	private String hSNCode;

	@SerializedName("pro_status")
	private String proStatus;

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setMrp(double mrp){
		this.mrp = mrp;
	}

	public double getMrp(){
		return mrp;
	}

	public void setOldMrp(double oldMrp){
		this.oldMrp = oldMrp;
	}

	public double getOldMrp(){
		return oldMrp;
	}

	public void setProExpIamge(String proExpIamge){
		this.proExpIamge = proExpIamge;
	}

	public String getProExpIamge(){
		return proExpIamge;
	}

	public void setAppId(String appId){
		this.appId = appId;
	}

	public String getAppId(){
		return appId;
	}

	public void setPlant(String plant){
		this.plant = plant;
	}

	public String getPlant(){
		return plant;
	}

	public void setProType(String proType){
		this.proType = proType;
	}

	public String getProType(){
		return proType;
	}

	public void setExpoledview(int expoledview){
		this.expoledview = expoledview;
	}

	public int getExpoledview(){
		return expoledview;
	}

	public void setProductname(String productname){
		this.productname = productname;
	}

	public String getProductname(){
		return productname;
	}

	public void setOemPartno(String oemPartno){
		this.oemPartno = oemPartno;
	}

	public String getOemPartno(){
		return oemPartno;
	}

	public void setPartVolt(String partVolt){
		this.partVolt = partVolt;
	}

	public String getPartVolt(){
		return partVolt;
	}

	public void setUnittype(String unittype){
		this.unittype = unittype;
	}

	public String getUnittype(){
		return unittype;
	}

	public void setGST(int gST){
		this.gST = gST;
	}

	public int getGST(){
		return gST;
	}

	public void setFullUnitNo(String fullUnitNo){
		this.fullUnitNo = fullUnitNo;
	}

	public String getFullUnitNo(){
		return fullUnitNo;
	}

	public void setPartOutputrng(String partOutputrng){
		this.partOutputrng = partOutputrng;
	}

	public String getPartOutputrng(){
		return partOutputrng;
	}

	public void setOEname(String oEname){
		this.oEname = oEname;
	}

	public String getOEname(){
		return oEname;
	}

	public void setOemId(String oemId){
		this.oemId = oemId;
	}

	public String getOemId(){
		return oemId;
	}

	public void setProId(String proId){
		this.proId = proId;
	}

	public String getProId(){
		return proId;
	}

	public void setProSupersed(String proSupersed){
		this.proSupersed = proSupersed;
	}

	public String getProSupersed(){
		return proSupersed;
	}

	public void setEnginType(String enginType){
		this.enginType = enginType;
	}

	public String getEnginType(){
		return enginType;
	}

	public void setPartNo(String partNo){
		this.partNo = partNo;
	}

	public String getPartNo(){
		return partNo;
	}

	public void setUos(String uos){
		this.uos = uos;
	}

	public String getUos(){
		return uos;
	}

	public void setProImage(String proImage){
		this.proImage = proImage;
	}

	public String getProImage(){
		return proImage;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setProModel(String proModel){
		this.proModel = proModel;
	}

	public String getProModel(){
		return proModel;
	}

	public void setAppname(String appname){
		this.appname = appname;
	}

	public String getAppname(){
		return appname;
	}

	public void setHSNCode(String hSNCode){
		this.hSNCode = hSNCode;
	}

	public String getHSNCode(){
		return hSNCode;
	}

	public void setProStatus(String proStatus){
		this.proStatus = proStatus;
	}

	public String getProStatus(){
		return proStatus;
	}
}