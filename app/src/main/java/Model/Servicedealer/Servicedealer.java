
package Model.Servicedealer;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Servicedealer {

    @SerializedName("data")
    private Servisedealerdetails mServisedealerdetails;
    @SerializedName("result")
    private String mResult;

    public Servisedealerdetails getData() {
        return mServisedealerdetails;
    }

    public void setData(Servisedealerdetails servisedealerdetails) {
        mServisedealerdetails = servisedealerdetails;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
