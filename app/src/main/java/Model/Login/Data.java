
package Model.Login;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("City")
    private String mCity;
    @SerializedName("Company")
    private String mCompany;
    @SerializedName("Email_id")
    private String mEmailId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("Rid")
    private String mRid;

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String Company) {
        mCompany = Company;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String EmailId) {
        mEmailId = EmailId;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String InsertDate) {
        mInsertDate = InsertDate;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String MobileNo) {
        mMobileNo = MobileNo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String Name) {
        mName = Name;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String Password) {
        mPassword = Password;
    }

    public String getRid() {
        return mRid;
    }

    public void setRid(String Rid) {
        mRid = Rid;
    }

}
