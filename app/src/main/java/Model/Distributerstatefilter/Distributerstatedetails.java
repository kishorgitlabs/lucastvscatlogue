
package Model.Distributerstatefilter;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
@Entity(tableName = "distributors")
public class Distributerstatedetails {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @SerializedName("address")
    @ColumnInfo(name = "address")
    private String mAddress;
    @SerializedName("city")
    @ColumnInfo(name = "city")
    private String mCity;
    @SerializedName("Company")
    @ColumnInfo(name = "Company")
    private String mCompany;
    @SerializedName("Contactperson")
    @ColumnInfo(name = "Contactperson")
    private String mContactperson;
    @SerializedName("customercode")
    @ColumnInfo(name = "customercode")
    private String mCustomercode;
    @SerializedName("Email_id")
    @ColumnInfo(name = "Email_id")
    private String mEmailId;
    @SerializedName("Landlineno")
    @ColumnInfo(name = "Landlineno")
    private String mLandlineno;
    @SerializedName("Mobileno")
    @ColumnInfo(name = "Mobileno")
    private String mMobileno;
    @SerializedName("region")
    @ColumnInfo(name = "region")
    private String mRegion;
    @SerializedName("state")
    @ColumnInfo(name = "state")
    private String mState;
    @SerializedName("TotalAvg")
    @ColumnInfo(name = "TotalAvg")
    @Ignore
    private Long mTotalAvg;
    @SerializedName("TotalRating")
    @ColumnInfo(name = "TotalRating")
    @Ignore
    private Long mTotalRating;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String Company) {
        mCompany = Company;
    }

    public String getContactperson() {
        return mContactperson;
    }

    public void setContactperson(String Contactperson) {
        mContactperson = Contactperson;
    }

    public String getCustomercode() {
        return mCustomercode;
    }

    public void setCustomercode(String customercode) {
        mCustomercode = customercode;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String EmailId) {
        mEmailId = EmailId;
    }

    public String getLandlineno() {
        return mLandlineno;
    }

    public void setLandlineno(String Landlineno) {
        mLandlineno = Landlineno;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String Mobileno) {
        mMobileno = Mobileno;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String region) {
        mRegion = region;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public Long getTotalAvg() {
        return mTotalAvg;
    }

    public void setTotalAvg(Long TotalAvg) {
        mTotalAvg = TotalAvg;
    }

    public Long getTotalRating() {
        return mTotalRating;
    }

    public void setTotalRating(Long TotalRating) {
        mTotalRating = TotalRating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
