
package Model.FullUnitsearch;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class SerFullunitsearch {

    @SerializedName("data")
    private List<Serfullunitsearchlist> mData;
    @SerializedName("result")
    private String mResult;

    public List<Serfullunitsearchlist> getData() {
        return mData;
    }

    public void setData(List<Serfullunitsearchlist> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
