
package Model.FullUnitsearch;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Serfullunitsearchlist {

    @SerializedName("Ser_Childpartno")
    private String mSerChildpartno;
    @SerializedName("Ser_Desc")
    private String mSerDesc;
    @SerializedName("Ser_ilno")
    private String mSerIlno;
    @SerializedName("Ser_noff")
    private String mSerNoff;
    @SerializedName("Ser_parno")
    private String mSerParno;
    @SerializedName("Ser_proimage")
    private String mSerProimage;
    @SerializedName("sid")
    private String mSid;

    public String getSerChildpartno() {
        return mSerChildpartno;
    }

    public void setSerChildpartno(String SerChildpartno) {
        mSerChildpartno = SerChildpartno;
    }

    public String getSerDesc() {
        return mSerDesc;
    }

    public void setSerDesc(String SerDesc) {
        mSerDesc = SerDesc;
    }

    public String getSerIlno() {
        return mSerIlno;
    }

    public void setSerIlno(String SerIlno) {
        mSerIlno = SerIlno;
    }

    public String getSerNoff() {
        return mSerNoff;
    }

    public void setSerNoff(String SerNoff) {
        mSerNoff = SerNoff;
    }

    public String getSerParno() {
        return mSerParno;
    }

    public void setSerParno(String SerParno) {
        mSerParno = SerParno;
    }

    public String getSerProimage() {
        return mSerProimage;
    }

    public void setSerProimage(String SerProimage) {
        mSerProimage = SerProimage;
    }

    public String getSid() {
        return mSid;
    }

    public void setSid(String sid) {
        mSid = sid;
    }

}
