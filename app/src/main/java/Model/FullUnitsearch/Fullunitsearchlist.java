
package Model.FullUnitsearch;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Fullunitsearchlist {

    @SerializedName("C_Id")
    private Long mCId;
    @SerializedName("childparts")
    private String mChildparts;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("illno")
    private String mIllno;
    @SerializedName("illno_sno")
    private String mIllnoSno;
    @SerializedName("manufacturername")
    private String mManufacturername;
    @SerializedName("moq")
    private String mMoq;
    @SerializedName("mrp")
    private String mMrp;
    @SerializedName("partno")
    private String mPartno;

    public Long getCId() {
        return mCId;
    }

    public void setCId(Long CId) {
        mCId = CId;
    }

    public String getChildparts() {
        return mChildparts;
    }

    public void setChildparts(String childparts) {
        mChildparts = childparts;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getIllno() {
        return mIllno;
    }

    public void setIllno(String illno) {
        mIllno = illno;
    }

    public String getIllnoSno() {
        return mIllnoSno;
    }

    public void setIllnoSno(String illnoSno) {
        mIllnoSno = illnoSno;
    }

    public String getManufacturername() {
        return mManufacturername;
    }

    public void setManufacturername(String manufacturername) {
        mManufacturername = manufacturername;
    }

    public String getMoq() {
        return mMoq;
    }

    public void setMoq(String moq) {
        mMoq = moq;
    }

    public String getMrp() {
        return mMrp;
    }

    public void setMrp(String mrp) {
        mMrp = mrp;
    }

    public String getPartno() {
        return mPartno;
    }

    public void setPartno(String partno) {
        mPartno = partno;
    }

}
