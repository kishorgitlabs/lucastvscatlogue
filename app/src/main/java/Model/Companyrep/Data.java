package Model.Companyrep;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Data{
  @SerializedName("password")
  @Expose
  private String password;
  @SerializedName("address")
  @Expose
  private String address;
  @SerializedName("access")
  @Expose
  private String access;
  @SerializedName("city")
  @Expose
  private String city;
  @SerializedName("company_name")
  @Expose
  private String company_name;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("emailid")
  @Expose
  private String emailid;
  @SerializedName("designation")
  @Expose
  private String designation;
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("phoneno")
  @Expose
  private String phoneno;
  @SerializedName("bulletin_id")
  @Expose
  private String bulletin_id;
  @SerializedName("status")
  @Expose
  private String status;
  public void setPassword(String password){
   this.password=password;
  }
  public String getPassword(){
   return password;
  }
  public void setAddress(String address){
   this.address=address;
  }
  public String getAddress(){
   return address;
  }
  public void setAccess(String access){
   this.access=access;
  }
  public String getAccess(){
   return access;
  }
  public void setCity(String city){
   this.city=city;
  }
  public String getCity(){
   return city;
  }
  public void setCompany_name(String company_name){
   this.company_name=company_name;
  }
  public String getCompany_name(){
   return company_name;
  }
  public void setName(String name){
   this.name=name;
  }
  public String getName(){
   return name;
  }
  public void setEmailid(String emailid){
   this.emailid=emailid;
  }
  public String getEmailid(){
   return emailid;
  }
  public void setDesignation(String designation){
   this.designation=designation;
  }
  public String getDesignation(){
   return designation;
  }
  public void setId(String id){
   this.id=id;
  }
  public String getId(){
   return id;
  }
  public void setPhoneno(String phoneno){
   this.phoneno=phoneno;
  }
  public String getPhoneno(){
   return phoneno;
  }
  public void setBulletin_id(String bulletin_id){
   this.bulletin_id=bulletin_id;
  }
  public String getBulletin_id(){
   return bulletin_id;
  }
  public void setStatus(String status){
   this.status=status;
  }
  public String getStatus(){
   return status;
  }
}