
package Model.Dealercityfilter;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Dealercitydetails {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Address1")
    private String mAddress1;
    @SerializedName("Allocation")
    private Object mAllocation;
    @SerializedName("AuditorName_admin")
    private Object mAuditorNameAdmin;
    @SerializedName("AuditorName_service")
    private Object mAuditorNameService;
    @SerializedName("AuditorName_tool")
    private Object mAuditorNameTool;
    @SerializedName("AuditorName_warrenty")
    private Object mAuditorNameWarrenty;
    @SerializedName("AuditorName_work")
    private Object mAuditorNameWork;
    @SerializedName("Category")
    private String mCategory;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Contactperson")
    private String mContactperson;
    @SerializedName("Date")
    private Object mDate;
    @SerializedName("Dealerid")
    private String mDealerid;
    @SerializedName("Dealername")
    private String mDealername;
    @SerializedName("Depocode")
    private String mDepocode;
    @SerializedName("Depotname")
    private String mDepotname;
    @SerializedName("Email_id")
    private String mEmailId;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Landlineno")
    private String mLandlineno;
    @SerializedName("Mobileno")
    private String mMobileno;
    @SerializedName("Pincode")
    private Long mPincode;
    @SerializedName("Region")
    private String mRegion;
    @SerializedName("Reportingtvsgroup")
    private String mReportingtvsgroup;
    @SerializedName("sno")
    private Object mSno;
    @SerializedName("State")
    private String mState;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("TotalAvg")
    private Long mTotalAvg;
    @SerializedName("TotalRating")
    private Long mTotalRating;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getAddress1() {
        return mAddress1;
    }

    public void setAddress1(String Address1) {
        mAddress1 = Address1;
    }

    public Object getAllocation() {
        return mAllocation;
    }

    public void setAllocation(Object Allocation) {
        mAllocation = Allocation;
    }

    public Object getAuditorNameAdmin() {
        return mAuditorNameAdmin;
    }

    public void setAuditorNameAdmin(Object AuditorNameAdmin) {
        mAuditorNameAdmin = AuditorNameAdmin;
    }

    public Object getAuditorNameService() {
        return mAuditorNameService;
    }

    public void setAuditorNameService(Object AuditorNameService) {
        mAuditorNameService = AuditorNameService;
    }

    public Object getAuditorNameTool() {
        return mAuditorNameTool;
    }

    public void setAuditorNameTool(Object AuditorNameTool) {
        mAuditorNameTool = AuditorNameTool;
    }

    public Object getAuditorNameWarrenty() {
        return mAuditorNameWarrenty;
    }

    public void setAuditorNameWarrenty(Object AuditorNameWarrenty) {
        mAuditorNameWarrenty = AuditorNameWarrenty;
    }

    public Object getAuditorNameWork() {
        return mAuditorNameWork;
    }

    public void setAuditorNameWork(Object AuditorNameWork) {
        mAuditorNameWork = AuditorNameWork;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String Category) {
        mCategory = Category;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public String getContactperson() {
        return mContactperson;
    }

    public void setContactperson(String Contactperson) {
        mContactperson = Contactperson;
    }

    public Object getDate() {
        return mDate;
    }

    public void setDate(Object Date) {
        mDate = Date;
    }

    public String getDealerid() {
        return mDealerid;
    }

    public void setDealerid(String Dealerid) {
        mDealerid = Dealerid;
    }

    public String getDealername() {
        return mDealername;
    }

    public void setDealername(String Dealername) {
        mDealername = Dealername;
    }

    public String getDepocode() {
        return mDepocode;
    }

    public void setDepocode(String Depocode) {
        mDepocode = Depocode;
    }

    public String getDepotname() {
        return mDepotname;
    }

    public void setDepotname(String Depotname) {
        mDepotname = Depotname;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String EmailId) {
        mEmailId = EmailId;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLandlineno() {
        return mLandlineno;
    }

    public void setLandlineno(String Landlineno) {
        mLandlineno = Landlineno;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String Mobileno) {
        mMobileno = Mobileno;
    }

    public Long getPincode() {
        return mPincode;
    }

    public void setPincode(Long Pincode) {
        mPincode = Pincode;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String Region) {
        mRegion = Region;
    }

    public String getReportingtvsgroup() {
        return mReportingtvsgroup;
    }

    public void setReportingtvsgroup(String Reportingtvsgroup) {
        mReportingtvsgroup = Reportingtvsgroup;
    }

    public Object getSno() {
        return mSno;
    }

    public void setSno(Object sno) {
        mSno = sno;
    }

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String Status) {
        mStatus = Status;
    }

    public Long getTotalAvg() {
        return mTotalAvg;
    }

    public void setTotalAvg(Long TotalAvg) {
        mTotalAvg = TotalAvg;
    }

    public Long getTotalRating() {
        return mTotalRating;
    }

    public void setTotalRating(Long TotalRating) {
        mTotalRating = TotalRating;
    }

}
