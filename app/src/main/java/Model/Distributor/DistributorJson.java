package Model.Distributor;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DistributorJson{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private List<String> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<String> data){
		this.data = data;
	}

	public List<String> getData(){
		return data;
	}
}